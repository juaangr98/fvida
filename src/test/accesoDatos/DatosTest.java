/** 
Proyecto: Juego de la vida.
 * Pruebas del almacén de datos.
 * @since: prototipo 0.1.1
 * @source: DatosTest.java 
 * @version: 0.1.1 - 2020/01/10
 * @author: JGR
 */

package accesoDatos;

import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.ModeloException;
import modelo.Nif;
import modelo.Usuario;
import util.Fecha;
import util.Formato;

public class DatosTest {

	private static Datos almacen;
	
	public DatosTest() {
		almacen = Datos.getAlmacen();
	}
	
	/**
	 * Genera datos de prueba válidos dentro 
	 * del almacén de datos.
	 * @throws ModeloException 
	 */
	public void cargarUsuariosPrueba() {
		String letrasNif = Formato.LETRAS_NIF; 	

		try {
			for (int i=3; i < 7; i++) {
				Usuario usuario = new Usuario(new Nif("0000000" + i + letrasNif.charAt(i)), 
						"Pepe", "López Pérez", 
						new DireccionPostal("Iglesia", "12", "30012", "Patiño"), 
						new Correo("pepe"+ i +"@gmail.com"),
						new Fecha(1998, 3, 21), 
						new Fecha(2019, 11, 17), 
						new ClaveAcceso("Miau#" + i),
						Usuario.RolUsuario.NORMAL);
				almacen.altaUsuario(usuario);
			}
		} catch (ModeloException e) {
			e.printStackTrace();
		}	
	}
	
	public static void main(String[] args) {
		
			DatosTest prueba = new DatosTest();
			prueba.cargarUsuariosPrueba();
			almacen.mostrarTodosUsuarios();
//			
//			try {
//			Usuario admin = new Usuario(new Nif("00000001R"),
//					"Admin", 
//					"Admin Admin",
//					new DireccionPostal(),
//					new Correo("admin@correo.es"),
//					new Fecha().addYears(-16),
//					new Fecha(),
//					new ClaveAcceso("Miau#0"),
//					Usuario.RolUsuario.ADMIN
//					);
//					almacen.altaUsuario(admin);
//		} catch (ModeloException e) {
//			e.printStackTrace();
//		}
//				
//		try {
//			Usuario usuario = new Usuario(new Nif("00000002W"), 
//					"Pepe", "López Pérez", 
//					new DireccionPostal("Iglesia", "12", "30012", "Patiño"), 
//					new Correo("pepe2@gmail.com"),
//					new Fecha(1998, 3, 21), 
//					new Fecha(2019, 11, 17), 
//					new ClaveAcceso("Miau#2"),
//					Usuario.RolUsuario.NORMAL);
//			almacen.altaUsuario(usuario);
//			
//			almacen.mostrarTodosUsuarios();
//		} catch (ModeloException e) {
//			e.printStackTrace();
//		}
	}
}
