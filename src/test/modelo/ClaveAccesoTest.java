/** 
 * Proyecto: Juego de la vida.
 * Prueba de la clase Claveacceso según el modelo 1.1.
 * @since: prototipo 0.1.1
 * @source: ClaveAccesoTest.java 
 * @version: 0.1.1 - 2020.02.10
 * @author: ajp
 */

package modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.ClaveAcceso;

public class ClaveAccesoTest {
	private ClaveAcceso clave1;
	private ClaveAcceso clave2 ;
	
	
	@Before
	public void iniciarlizarDatosPrueba() {	
		try {
			clave1 = new ClaveAcceso();
			clave2 = new ClaveAcceso("Miau#2");
		} 
		catch (Exception e) {	}
	}
	
	@After
	public void borrarDatosPrueba() {	
		clave1 = null;
	}
	
	// Test con DATOS VALIDOS
	@Test
	public void testClaveAccesoConvencional() {	
		assertEquals(clave2.getTexto(), new ClaveAcceso("Miau#2").getTexto());
	}

	@Test
	public void testClaveAcceso() {
		assertEquals(clave1.getTexto(), new ClaveAcceso("Miau#0").getTexto());
	}

	@Test
	public void testClaveAccesoClaveAcceso() {
		try {
			assertEquals(clave2, new ClaveAcceso(clave2));
		} 
		catch (Exception e) {	}
		
	}

	@Test
	public void testSetTexto() {
		try {
			clave1.setTexto("Miau#1");
			assertEquals(clave1.getTexto(), new ClaveAcceso("Miau#1").getTexto());
		} 
		catch (Exception e) {	}
		
	}

	@Test
	public void testToString() {
		assertEquals(clave2.toString(), new ClaveAcceso("Miau#2").getTexto());
	}

	@Test
	public void testEquals() {
		try {
			assertTrue(clave2.equals(new ClaveAcceso("Miau#2")));
		} 
		catch (Exception e) { }
		
	}

	@Test
	public void testClone() {
		assertEquals(clave2, clave2.clone());
	}

	@Test
	public void testHashCode() {
		assertEquals(clave2.hashCode(), -1900842130);
	}

	// Test con DATOS NO VALIDOS
	
	@Test
	public void testSetTextoNull() {
		try {
			clave1.setTexto(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
			assertTrue(true);
		}
	}
	
	@Test
	public void testSetTextoMalFormato() {
		try {
			clave1.setTexto("hola");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
			assertTrue(true);
		}
	}
	
} 
