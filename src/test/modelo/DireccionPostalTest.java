/** Proyecto: Juego de la vida.
 *  Prueba de la clase DireccionPostal según el modelo 1.1
 *  @since: prototipo0.1.1
 *  @source: DireccionPostalTest.java 
 *  @version: 0.1.1 - 2020/02/10
 *  @author: ajp
 */

package modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.DireccionPostal;

public class DireccionPostalTest {

	private DireccionPostal direccion1;
	private DireccionPostal direccion2;

	/**
	 * Método que se ejecuta antes de cada @Test para preparar datos de prueba.
	 */
	@Before
	public void iniciarlizarDatosPrueba() {
		// Objetos para la prueba.
		try {
			direccion1 = new DireccionPostal();
			direccion2 = new DireccionPostal("Flan", "21", "88888", "Murcia");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método que se ejecuta después de cada @Test para limpiar datos de prueba.
	 */
	@After
	public void borrarDatosPrueba() {
		direccion1 = null;
		direccion2 = null;
	}

	// Test con DATOS VALIDOS
	@Test
	public void testDireccionPostalConvencional() {
		assertEquals(direccion2.getCalle(), "Flan");
		assertEquals(direccion2.getNumero(), "21");
		assertEquals(direccion2.getCp(), "88888");
		assertEquals(direccion2.getPoblacion(), "Murcia");
	}

	@Test
	public void testDireccionPostal() {
		assertEquals(direccion1, new DireccionPostal("Calle", "00", "00000", "Población"));
	}

	@Test
	public void testDireccionPostalDireccionPostal() {
		try {
			assertEquals(direccion2, new DireccionPostal(direccion2));
		} 
		catch (Exception e) {	}
	}
	

	
	@Test
	public void testSetCalle() {
		try {
			direccion1.setCalle("Calle");
		} catch (Exception e) { }	
		assertEquals(direccion1.getCalle(), "Calle");
	}
	
	@Test
	public void testSetNumero() {
		try {
			direccion1.setNumero("00");
		} catch (Exception e) { }	
		assertEquals(direccion1.getNumero(), "00");
	}
	
	@Test
	public void testSetCP() {
		try {
			direccion1.setCp("99999");
		} catch (Exception e) { }	
		assertEquals(direccion1.getCp(), "99999");
	}
	
	@Test
	public void testSetPoblacion() {
		try {
			direccion1.setPoblacion("Población");
		} catch (Exception e) { }
		assertEquals(direccion1.getPoblacion(), "Población");
	}
	
	@Test
	public void testToString() {
		assertEquals(direccion2.toString(), "Flan, 21, 88888, Murcia");
	}
	
	@Test
	public void testEqualsObject() {
		try {		
			assertTrue(direccion1.equals(new DireccionPostal()));
		} 
		catch (Exception e) {	}
	}

	@Test
	public void testClone() {
		assertEquals(direccion2, direccion2.clone());
	}
	
	// Test con DATOS NO VALIDOS
	@Test
	public void testCorreoConvencionalCalleNull() {	

		try {
			new DireccionPostal(null, "21", "88888", "Murcia");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testCorreoConvencionalNumeroNull() {	

		try {
			new DireccionPostal("Flan", null, "88888", "Murcia");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCorreoConvencionalCPNull() {	

		try {
			new DireccionPostal("Flan", "21", null, "Murcia");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCorreoConvencionalPoblacionNull() {	

		try {
			new DireccionPostal("Flan", "21", "88888", null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testSetCalleNull() {
		try {
			direccion1.setCalle(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testSetNumeroNull() {
		try {
			direccion1.setNumero(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testSetCPNull() {
		try {
			direccion1.setCp(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testSetPoblacionNull() {
		try {
			direccion1.setPoblacion(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError | Exception e) {
			assertTrue(true);
		}
	}
	
} // class