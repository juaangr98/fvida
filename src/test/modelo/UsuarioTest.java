/** 
 * Proyecto: Juego de la vida.
 * Clase JUnit5 de prueba automatizada de las características de la clase Usuario según el modelo 1.
 * @since: prototipo 0.1.0
 * @source: UsuarioTest.java 
 * @version: 0.1.0 - 2019.12.17
 * @author: ajp
 */

package modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import util.Fecha;

import java.util.Date;

public class UsuarioTest {
	private static Usuario usuario1; 
	private Usuario usuario2; 

	/**
	 * Método que se ejecuta antes de cada @Test para preparar datos de prueba.
	 */
	@BeforeAll
	public static void iniciarlizarDatosFijos() {
		// Objetos no modicados en las pruebas.
		usuario1 = new Usuario(new Nif("00000001R"), 
				"Luis", "Roca Mora",
				new DireccionPostal("Roncal", "10", "30130", "Murcia"), 
				new Correo("luis@gmail.com"), 
				new Fecha(2000, 3, 21),
				new Fecha(2019, 11, 17), 
				new ClaveAcceso("Miau#12"), 
				Usuario.RolUsuario.ADMIN);
	}

	/**
	 * Método que se ejecuta una sola vez al final del conjunto pruebas.
	 * No es necesario en este caso.
	 */
	@AfterAll
	public static void limpiarDatosFijos() {
		usuario1 = null;
	}

	/**
	 * Método que se ejecuta antes de cada pruebas.
	 */
	@BeforeEach
	public void iniciarlizarDatosVariables() {	
		usuario2 = new Usuario();
	}

	/**
	 * Método que se ejecuta después de cada @Test para limpiar datos de prueba.
	 */
	@AfterEach
	public void borrarDatosPrueba() {
		usuario2 = null;
	}

	// Test's CON DATOS VALIDOS
	
	@Test
	public void testSetNif() {
		usuario2.setNif(new Nif("00000001R"));
		assertEquals(usuario2.getNif(), new Nif("00000001R"));
	}
	
	@Test
	public void testSetNombre() {
		usuario2.setNombre("Luis");
		assertEquals(usuario2.getNombre(), "Luis");
	}
	
	@Test
	public void testSetApellidos() {
		usuario2.setApellidos("Roca Mora");
		assertEquals(usuario2.getApellidos(), "Roca Mora");
	}
	
	@Test
	public void testSetDomicilio() {
		usuario2.setDomicilio(new DireccionPostal("Roncal", "10", "30130", "Murcia"));
		assertEquals(usuario2.getDomicilio(), new DireccionPostal("Roncal", "10", "30130", "Murcia"));
	}
	
	@Test
	public void testSetCorreo() {
		usuario2.setCorreo(new Correo("luis@gmail.com"));
		assertEquals(usuario2.getCorreo(), new Correo("luis@gmail.com"));
	}
	@Test
	public void testSetFechaNacimiento() {
		usuario2.setFechaNacimiento(new Fecha(2000, 3, 21));
		assertEquals(usuario2.getFechaNacimiento(), new Fecha(2000, 3, 21));
	}
	
	@Test
	public void testSetFechaAlta() {
		usuario2.setFechaAlta(new Fecha(2019, 11, 17));
		assertEquals(usuario2.getFechaAlta(), new Fecha(2019, 11, 17));
	}

	@Test
	public void testSetClaveAcceso() {
		usuario2.setClaveAcceso(new ClaveAcceso("Miau#12"));
		assertEquals(usuario2.getClaveAcceso(), new ClaveAcceso("Miau#12"));
	}

	@Test
	public void testSetRol() {
		usuario2.setRol(Usuario.RolUsuario.NORMAL);
		assertEquals(usuario2.getRol(), Usuario.RolUsuario.NORMAL);
	}

	@Test
	public void testToString() {
		assertEquals(usuario1.toString(), 
				"nif:             00000001R\n" +
				"nombre:          Luis\n" +
				"apellidos:       Roca Mora\n" +
				"domicilio:       Roncal, 10, 30130, Murcia\n" +
				"correo:          luis@gmail.com\n" +
				"fechaNacimiento: 2000.3.21\n" +
				"id:              LRM1R\n" +
				"fechaAlta:       2019.11.17\n" +
				"claveAcceso:     Pmezd9!\n" +
				"rol:             ADMIN\n"
			);
	}

	// Test's CON DATOS NO VALIDOS

	@Test
	public void testSetNombreNull() {
		try {
			usuario2.setNombre(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetApellidosNull() {
		try {
			usuario2.setApellidos(null);
			
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetDomicilioNull() {
		try {
			usuario2.setDomicilio(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetCorreoNull() {
		try {
			usuario2.setCorreo(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetFechaNacimientoNull() {
		try {
			usuario2.setFechaNacimiento(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetFechaNacimientoFuturo() {	
			usuario1.setFechaNacimiento(new Fecha(3020, 9, 10));
			// No debe haber cambios...
			assertEquals(usuario1.getFechaNacimiento(), new Fecha(2000, 3, 21));
	}
	
	@Test
	public void testSetFechaAltaNull() {
		try {
			usuario2.setFechaAlta(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) {	
		}
	}

	@Test
	public void testSetFechaAltaFuturo() {	
			usuario1.setFechaAlta(new Fecha(3020, 9, 10));
			// No debe haber cambios...
			assertEquals(usuario1.getFechaAlta(), new Fecha(2019, 11, 17));
	}
	
	@Test
	public void testSetClaveAccesoNull() {
		try {
			usuario2.setClaveAcceso(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetRolNull() {
		try {
			usuario2.setRol(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 		
		}
	}

} // class
