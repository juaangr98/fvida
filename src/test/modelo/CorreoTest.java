/** Proyecto: Juego de la vida.
 *  Prueba de la clase Correo según el modelo 1.1
 *  @since: prototipo2.0
 *  @source: CorreoTest.java 
 *  @version: 0.1.1 - 2020/02/10
 *  @author: ajp
 */

package modelo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Correo;

public class CorreoTest {
	private Correo correo1;
	private Correo correo2;

	public CorreoTest () {
		try {
			correo2 = new Correo("correo@correo.com");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void iniciarlizarDatosPrueba() {
		try {
			correo1 = new Correo();
			correo2 = new Correo("correo@correo.com");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void borrarDatosPrueba() {
		correo1 = null;
	}
	
	// Test con DATOS VALIDOS
	@Test
	public void testCorreoConvencional() {
		assertEquals(correo2.getTexto(), "correo@correo.com");
	}

	@Test
	public void testCorreo() {
		assertEquals(correo1, new Correo("invitado@correo.es"));
	}

	@Test
	public void testCorreoCorreo() {
		assertNotSame(correo2, new Correo(correo2));
	}
	
	@Test
	public void testSetTexto() {
		try {
			correo2.setTexto("correo@correo.com");
		} 
		catch (Exception e) {	}
		assertEquals(correo2.getTexto(), "correo@correo.com");
	}

	@Test
	public void testToString() {
		assertEquals("correo@correo.com", correo2.toString());
	}
	
	@Test
	public void testEqualsObject() {
		try {		
			assertTrue(correo2.equals(new Correo("correo@correo.com")));
		} 
		catch (Exception e) {	}
	}

	@Test
	public void testClone() {
		assertEquals(correo2, correo2.clone());
	}

	@Test
	public void testHashCode() {
		assertEquals(correo1.hashCode(), 805823093);
	}
	
	// Test con DATOS NO VALIDOS

		@Test
		public void testSetTextoNull() {
			try {
				correo1.setTexto(null);
				fail("No debe llegar aquí...");
			} 
			catch (AssertionError e) {
				assertTrue(true);
			}
		}
		
} 
