/** 
 *  Proyecto: Juego de la vida.
 *  Clase-utilidad que adapta el uso de un Calendar para manejo de fechas en el programa.
 *  @since: prototipo 0.1.1
 *  @source: Fecha.java 
 *  @version: 0.1.2 - 2020/01/10
 *  @author: JGR
 */
package util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Fecha {

	// Objeto de la clase adaptada.
	private Calendar tiempo;

	/**
	 * Constructor defecto.
	 */
	public Fecha() {
		tiempo = new GregorianCalendar();
	}

	/**
	 * Constructor convencional adaptado.
	 */
	public Fecha(int año, int mes, int dia, int hora, int minuto, int seg) {
		tiempo = new GregorianCalendar(año, mes, dia, hora, minuto, seg);
	}
	
	/**
	 * Constructor convencional adaptado.
	 */
	public Fecha(int año, int mes, int dia) {
		this(año, mes-1, dia, 0, 0, 0);
	}
	
	public Fecha(Fecha fecha) {
		tiempo = (Calendar) fecha.tiempo.clone();
	}

	public Fecha(String fecha) {
		String[] partes = fecha.split("[/-.]");	
		int año = Integer.parseInt(partes[0]);
		int mes = Integer.parseInt(partes[1]);
		int dia = Integer.parseInt(partes[2]);
		tiempo = new GregorianCalendar(año, mes, dia);
	}
	
	//Métodos de acceso adaptados
	public int getYear() {
		return tiempo.get(Calendar.YEAR);
	}

	public int getMes() {
		return tiempo.get(GregorianCalendar.MONTH) + 1;
	}

	public int getDia() {
		return tiempo.get(GregorianCalendar.DAY_OF_MONTH);
	}

	public int getSegundo() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMinuto() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getHora() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setYear(int año) {
		tiempo.set(GregorianCalendar.YEAR, año);
	}

	public void setMes(int mes) {
		tiempo.set(GregorianCalendar.YEAR, mes);
	}	

	public void setDia(int dia) {
		tiempo.set(GregorianCalendar.DAY_OF_MONTH, dia);
	}

	public boolean after(Fecha fecha) {
		return tiempo.after(fecha.tiempo);
	}
	
	public boolean before(Fecha fecha) {
		return tiempo.before(fecha.tiempo);
	}

	public Fecha addYears(int cantidad) {
		tiempo.add(Calendar.YEAR, cantidad);
		return this;
	}

	//...
	
	public int difYears(Fecha fecha) {
		return (int) ((this.tiempo.getTimeInMillis() 
				- fecha.tiempo.getTimeInMillis()) / (1000L * 60 * 60 * 24 * 365));
	}

	// ....
	
	public long marcaTiempoMilisegundos() {
		return this.tiempo.getTimeInMillis();
	}
	
	//...
	
	public String toStringMarcaTiempo() {
		return "" + this.getYear() + this.getMes() + this.getDia() 
					+ this.getHora() + this.getMinuto()+ this.getSegundo();
		
		
	}

	@Override
	public String toString() {
		return String.format("%4d.%02d.%02d", 
				getYear(), getMes(), getDia());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tiempo == null) ? 0 : tiempo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fecha other = (Fecha) obj;
		if (tiempo == null) {
			if (other.tiempo != null)
				return false;
		} else if (!tiempo.equals(other.tiempo))
			return false;
		return true;
	}
	
	@Override
	public Fecha clone(){
		return new Fecha(this);
	}
	
} // class
