/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de dirección postal según el modelo 1.1.
 *  En esta versión sólo se ha aplicado un diseño OO básico.
 *  @since: prototipo 0.1.1
 *  @source: DireccionPostal.java 
 *  @version: 0.1.2 - 2020/01/07
 *  @author: JGR
 */

package modelo;

import util.Formato;

public class DireccionPostal {
	private String calle;
	private String numero;
	private String cp;
	private String poblacion;

	public DireccionPostal(String calle, String numero, String cp, String poblacion) {
		this.setCalle(calle);
		this.setNumero(numero);
		this.setCp(cp);
		this.setPoblacion(poblacion);
	}

	public DireccionPostal() {
		this("Calle", "00", "00000", "Población");
	}

	public DireccionPostal(DireccionPostal direccion) {
		this(direccion.calle, direccion.numero, direccion.cp, direccion.poblacion);
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		if (calleValida(calle)) {
			this.calle = calle;
			return;
		} 
		throw new ModeloException ("DirrecioPostal.calle: null o Formato no válido...");	
	}

	/**
	 * Comprueba validez del calle.
	 * @param calle.
	 * @return true si cumple.
	 */
	private boolean calleValida(String calle) {
		return calle!= null && calle.matches(Formato.PATRON_NOMBRE_VIA);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		if (numeroValido(numero)) {
			this.numero = numero;
			return;
		} 
		throw new ModeloException ("DirrecioPostal.numero: null o Formato no válido...");
	}

	/**
	 * Comprueba validez numero.
	 * @param calle.
	 * @return true si cumple.
	 */
	private boolean numeroValido(String numero) {
		return numero!= null && numero.matches(Formato.PATRON_NUMERO_POSTAL);
	}


	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		if (cpValido(cp)) {
			this.cp = cp;
			return;
		} 
		throw new ModeloException ("DirrecioPostal.cp: null o Formato no válido...");
	}

	/**
	 * Comprueba validez cp.
	 * @param calle.
	 * @return true si cumple.
	 */
	private boolean cpValido(String cp) {
		return cp!= null && cp.matches(Formato.PATRON_CP);
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		if (poblacionValida(poblacion)) {
			this.poblacion = poblacion;
			return;
		} 
		throw new ModeloException ("DirrecioPostal.poblacion: null o Formato no válido...");
	}

	/**
	 * Comprueba validez poblacion.
	 * @param calle.
	 * @return true si cumple.
	 */
	private boolean poblacionValida(String cp) {
		return cp.matches(Formato.PATRON_TOPONIMO);
	}

	@Override
	public String toString() {
		return String.format("%s, %s, %s, %s", 
				calle, numero, cp, poblacion);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((calle == null) ? 0 : calle.hashCode());
		result = prime * result + ((cp == null) ? 0 : cp.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((poblacion == null) ? 0 : poblacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DireccionPostal other = (DireccionPostal) obj;
		if (calle == null) {
			if (other.calle != null)
				return false;
		} else if (!calle.equals(other.calle))
			return false;
		if (cp == null) {
			if (other.cp != null)
				return false;
		} else if (!cp.equals(other.cp))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (poblacion == null) {
			if (other.poblacion != null)
				return false;
		} else if (!poblacion.equals(other.poblacion))
			return false;
		return true;
	}

	@Override
	public DireccionPostal clone() {
		return new DireccionPostal(this);
	}

}
