/** 
 * Proyecto: Juego de la vida.
 * Implementa el concepto de Mundo según el modelo 1.2 
 * @since: prototipo 0.1.2
 * @source: Mundo.java 
 * @version: 0.1.2 - 2020/02/07
 * @author: JGR
 */
package modelo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import accesoUsr.Presentacion;


public class Mundo implements Identificable{


	//Reglas simulación
	private int[] constantesSobrevivir;
	private int[] constantesRenacer;

	private static final int DEFAULT_SPACE_SIZE = 18;
	private static final int CICLOS_SIMULACION = 20;
	
	
	public enum FormaEspacio {
		PLANO,
		ESFERICO;
	}
	
	private String nombre;
	private byte[][] espacio;
	private List <Posicion> distribuicion;
	private Map<String, int[]> constantes;
	private FormaEspacio tipoMundo;
	
	
	/**
	 * Constructor convencional.
	 * Establece el valor inicial de cada uno de los atributos.
	 * Recibe parámetros que se corresponden con los atributos.
	 * Utiliza métodos set... para la posible verificación.
	 * @param nombre
	 * @param fecha
	 * @param espacioMundo
	 * @param formaEspacio
	 */
	public Mundo(String nombre, byte[][] espacio, List<Posicion> distribuicion,
			FormaEspacio tipoMundo) {
		this.nombre = nombre;
		setEspacio(espacio);
		aplicarLeyesEstandar();
		this.tipoMundo = tipoMundo;
	}

	public Mundo() {
		this("Demo1",  
				new byte[DEFAULT_SPACE_SIZE][DEFAULT_SPACE_SIZE], 
				new ArrayList <Posicion>(),
				Mundo.FormaEspacio.PLANO
				);	
	}
	
	
	public Mundo(Mundo mundo) {

	}

	@Override
	public String getId() {
		return nombre;
	}
	
	
	@Override
	public String toString() {
		return String.format(
						  "%-16s %s\n"
						+ "%-16s %s\n"
						+ "%-16s %s\n"
						+ "%-16s %s\n"
						+ "%-16s %s\n",
						"Nombre:", nombre,	
						"espacio:", Arrays.toString(espacio),
						"constantesSobrevivir:", Arrays.toString(constantesSobrevivir),
						"constantesRenacer", Arrays.toString(constantesRenacer), 
						"distribuicion", distribuicion
				);
	}

	private void aplicarLeyesEstandar() {
		constantes = new HashMap<String, int[]>();
		constantes.put("constantesSobrevivir", new int[] {2, 3});
		constantes.put("constantesRenacer", new int[] {3});
	}
	
	public byte[][] getEspacio() {
		return espacio;
	}

	public void setEspacio(byte[][] espacio) {
		assert espacio != null;
		this.espacio = espacio;
	}

	public FormaEspacio getTipoMundo() {
		return tipoMundo;
	}

	public void setTipoMundo(FormaEspacio tipoMundo) {
		assert tipoMundo != null;
		this.tipoMundo = tipoMundo;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((constantes == null) ? 0 : constantes.hashCode());
		result = prime * result + Arrays.hashCode(constantesRenacer);
		result = prime * result + Arrays.hashCode(constantesSobrevivir);
		result = prime * result + ((distribuicion == null) ? 0 : distribuicion.hashCode());
		result = prime * result + Arrays.deepHashCode(espacio);
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((tipoMundo == null) ? 0 : tipoMundo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mundo other = (Mundo) obj;
		if (constantes == null) {
			if (other.constantes != null)
				return false;
		} else if (!constantes.equals(other.constantes))
			return false;
		if (!Arrays.equals(constantesRenacer, other.constantesRenacer))
			return false;
		if (!Arrays.equals(constantesSobrevivir, other.constantesSobrevivir))
			return false;
		if (distribuicion == null) {
			if (other.distribuicion != null)
				return false;
		} else if (!distribuicion.equals(other.distribuicion))
			return false;
		if (!Arrays.deepEquals(espacio, other.espacio))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (tipoMundo != other.tipoMundo)
			return false;
		return true;
	}
	
	/**
	 * Ejecuta una simulación del juego de la vida en la consola.
	 */
	public void lanzarSimulacion(Presentacion interfazUsr) {
		int generacion = 0; 
		do {
			interfazUsr.mostrarMundo(generacion);
			actualizarMundo();
			generacion++;
		}
		while (generacion < CICLOS_SIMULACION);
	}
	
	/**
	 * Actualiza el estado del Jue

go de la Vida.
	 * Actualiza según la configuración establecida para la forma del espacio.
	 */
	private void actualizarMundo() {
		if (tipoMundo == FormaEspacio.PLANO) {
			actualizarMundoPlano();
		}
		if (tipoMundo == FormaEspacio.ESFERICO) {
			actualizarMundoEsferico();
		}
	}

	/**
	 * Actualiza el estado almacenado del Juego de la Vida.
	 * Las celdas periféricas son adyacentes con las del lado opuesto.
	 * El mundo representado sería esférico cerrado sin límites para células de dos dimensiones.
	 */
	private void actualizarMundoEsferico()  {     					
		// Pendiente de implementar.
	}

	/**
	 * Actualiza el estado de cada celda almacenada del Juego de la Vida.
	 * Las celdas periféricas son los límites absolutos.
	 * El mundo representado sería plano, cerrado y con límites para células de dos dimensiones.
	 */
	private void actualizarMundoPlano()  {     					
		byte[][] nuevoEstado = new byte[espacio.length][espacio.length];

		for (int i = 0; i < espacio.length; i++) {
			for (int j = 0; j < espacio.length; j++) {
				int conteoVecinas = 0;							
				conteoVecinas += visitarCeldaNoroeste(i, j);		
				conteoVecinas += visitarCeldaNorte(i, j);			// 		NO | N | NE
				conteoVecinas += visitarCeldaNoreste(i, j);		//    	-----------
				conteoVecinas += visitarCeldaEste(i, j);			// 		 O | * | E
				conteoVecinas += visitarCeldaSureste(i, j);		// 	  	----------- 
				conteoVecinas += visitarCeldaSur(i, j); 			// 		SO | S | SE
				conteoVecinas += visitarCeldaSuroeste(i, j); 	  
				conteoVecinas += visitarCeldaOeste(i, j);		          			                                     	

				actualizarCelda(nuevoEstado, i, j, conteoVecinas);
			}
		}
		espacio = nuevoEstado;
	}
	
	/**
	 * Aplica las leyes del mundo a la celda indicada dada la cantidad de células adyacentes vivas.
	 * @param nuevoEstado
	 * @param fila
	 * @param col
	 * @param conteoVecinas
	 */
	private void actualizarCelda(byte[][] nuevoEstado, int fila, int col, int conteoVecinas) {	

		for (int valor : constantes.get("constantesRenacer")) {
			if (conteoVecinas == valor) {										// Pasa a estar viva.
				nuevoEstado[fila][col] = 1;
				return;
			}
		}

		for (int valor : constantes.get("constantesSobrevivir")) {
			if (conteoVecinas == valor && espacio[fila][col] == 1) {		// Permanece viva, si lo estaba.
				nuevoEstado[fila][col] = 1;
				return;
			}
		}
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Oeste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Oeste.
	 */
	private byte visitarCeldaOeste(int fila, int col) {
		if (col-1 >= 0) {
			return espacio[fila][col-1]; 			// Celda O.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Suroeste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Suroeste.
	 */
	private byte visitarCeldaSuroeste(int fila, int col) {
		if (fila+1 < espacio.length && col-1 >= 0) {
			return espacio[fila+1][col-1]; 		// Celda SO.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Sur de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Sur.
	 */
	private byte visitarCeldaSur(int fila, int col) {
		if (fila+1 < espacio.length) {
			return espacio[fila+1][col]; 			// Celda S.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Sureste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Sureste.
	 */
	private byte visitarCeldaSureste(int fila, int col) {
		if (fila+1 < espacio.length && col+1 < espacio.length) {
			return espacio[fila+1][col+1]; 		// Celda SE.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Este de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Este.
	 */
	private byte visitarCeldaEste(int fila, int col) {
		if (col+1 < espacio.length) {
			return espacio[fila][col+1]; 			// Celda E.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Noreste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Noreste.
	 */
	private byte visitarCeldaNoreste(int fila, int col) {
		if (fila-1 >= 0 && col+1 < espacio.length) {
			return espacio[fila-1][col+1]; 		// Celda NE.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al NO de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda NO.
	 */
	private byte visitarCeldaNorte(int fila, int col) {
		if (fila-1 >= 0) {
			return espacio[fila-1][col]; 			// Celda N.
		}
		return 0;
	}

	/**
	 * Obtiene el estado o valor de la celda vecina situada al Noroeste de la indicada por la coordenada. 
	 * @param fila de la celda evaluada.
	 * @param col de la celda evaluada.
	 * @return el estado o valor de la celda Noroeste.
	 */
	private byte visitarCeldaNoroeste(int fila, int col) {
		if (fila-1 >= 0 && col-1 >= 0) {
			return espacio[fila-1][col-1]; 		// Celda NO.
		}
		return 0;
	}
}
