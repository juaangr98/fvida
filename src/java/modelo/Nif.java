/** Proyecto: Juego de la vida.
 *  Implementa el concepto de Nif según el modelo 1.1
 *  @since: prototipo0.1.1
 *  @source: Nif.java 
 *  @version: 0.1.2 - 2020/01/20
 *  @author: JGR
 */

package modelo;

import util.Formato;

public class Nif {

	private String texto;

	public Nif(String texto) {
		setTexto(texto);
	}
	
	public Nif() {
		this("00000000T");
	}

	public Nif(Nif nif) {
		this(new String(nif.texto));
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto)  {
		if (nifValido(texto)) {
			this.texto = texto;
			return;
			} 
			throw new ModeloException ("Nif: null o Formato no válido...");
	}

	private boolean nifValido(String texto) {	
		return texto != null && texto.matches(Formato.PATRON_NIF)
				&& letraNIFValida(texto);
	}

	/**
	 * Comprueba la validez de la letra de un NIF
	 * @param texto del NIF
	 * @return true si la letra es correcta.
	 */
	private boolean letraNIFValida(String texto) {
		int valor = Integer.parseInt(texto.substring(0, 8));		
		return Formato.LETRAS_NIF.charAt(valor % 23) == texto.charAt(8);
		
	} 
	
	/**
	 * Reproduce el estado -valores de atributos- de objeto en forma de texto. 
	 * @return el texto formateado.  
	 */
	@Override
	public String toString() {
		return this.texto;
	}
	
	/**
	 * hashCode() complementa al método equals y sirve para comparar objetos de forma 
	 * rápida en estructuras Hash. 
	 * Cuando Java compara dos objetos en estructuras de tipo hash (HashMap, HashSet etc)
	 * primero invoca al método hashcode y luego el equals.
	 * @return un número entero de 32 bit.
	 */
	@Override
	public int hashCode() {
		final int primo = 31;
		int result = 1;
		result = primo * result + ((texto == null) ? 0 : texto.hashCode());
		return result;
	}

	/**
	 * Dos objetos son iguales si: 
	 * Son de la misma clase.
	 * Tienen los mismos valores en los atributos; o son el mismo objeto.
	 * @return falso si no cumple las condiciones.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj != null && getClass() == obj.getClass()) {
			if (this == obj) {
				return true;
			}
			if (texto.equals(((Nif) obj).texto)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Genera un clon del propio objeto realizando una copia profunda.
	 * @return el objeto clonado.
	 */
	@Override
	public Nif clone() {
		// Utiliza el constructor copia.
		return new Nif(this);
	}
	
}
