/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de contraseña según el modelo 1.1.
 *  @since: prototipo 0.1.1
 *  @source: ClaveAcceso.java 
 *  @version: 0.1.2 - 2020/01/07
 *  @author: JGR
 */

package modelo;

import util.Criptografia;
import util.Formato;

public class ClaveAcceso {

	private String texto;


	public ClaveAcceso(String texto) {
			setTexto(texto);
	}

	public ClaveAcceso() {
		this("Miau#0");
	}

	public ClaveAcceso(ClaveAcceso clave) {
		this.texto = clave.texto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		if (claveAccesoValida(texto)) {
			this.texto = Criptografia.cesar(texto);
			return;
		} 
		throw new ModeloException ("ClaveAcceso: null o formato no válido...");
	}

	/**
	 * Comprueba validez del nif.
	 * @param nif.
	 * @return true si cumple.
	 */
	private boolean claveAccesoValida(String texto) {
		return texto != null && texto.matches(Formato.PATRON_CONTRASEÑA);
	}

	@Override
	public String toString() {
		return String.format("%s", texto);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((texto == null) ? 0 : texto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClaveAcceso other = (ClaveAcceso) obj;
		if (texto == null) {
			if (other.texto != null)
				return false;
		} else if (!texto.equals(other.texto))
			return false;
		return true;
	}

	@Override
	public ClaveAcceso clone() {
		return new ClaveAcceso(this);
	}

}
