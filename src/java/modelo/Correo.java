/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de Correo según el modelo 1.1.
 *  @since: prototipo 0.1.1
 *  @source: Correo.java 
 *  @version: 0.1.2 - 2020/01/07
 *  @author: JGR
 */

package modelo;

import util.Formato;

public class Correo {

	private String texto;


	public Correo(String texto) {
		setTexto(texto);
	}

	public Correo() {
		this("invitado@correo.es");
	}

	public Correo(Correo correo) {
		this.texto = correo.texto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto){
		if (correoValido(texto)) {
			this.texto = texto;
			return;
		} 
		throw new ModeloException ("Correo: null o Formato no válido...");				// Defecto.
	}


	/**
	 * Comprueba validez del correo.
	 * @param correo.
	 * @return true si cumple.
	 */
	private boolean correoValido(String texto) {
		return texto!= null && texto.matches(Formato.PATRON_CORREO);
	}

	@Override
	public String toString() {
		return String.format("%s", texto);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((texto == null) ? 0 : texto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Correo other = (Correo) obj;
		if (texto == null) {
			if (other.texto != null)
				return false;
		} else if (!texto.equals(other.texto))
			return false;
		return true;
	}

	@Override
	public Correo clone() {
		return new Correo(this);
	}

}
