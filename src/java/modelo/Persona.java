/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de Persona según el modelo 1.2
 *  @since: prototipo 0.1.2
 *  @source: Persona.java 
 *  @version: 0.1.2 - 2020/01/7
 *  @author: JGR
 */

package modelo;

import modelo.Usuario.RolUsuario;
import util.Fecha;
import util.Formato;

public abstract class Persona {

	protected Nif nif;
	protected String nombre;
	protected String apellidos;
	protected DireccionPostal domicilio;
	protected Correo correo;
	protected Fecha fechaNacimiento;

	
	public Persona(Nif nif, String nombre, String apellidos, DireccionPostal domicilio, Correo correo,
			Fecha fechaNacimiento) {
		setNif(nif);
		setNombre(nombre);
		setApellidos(apellidos);
		setDomicilio(domicilio);
		setCorreo(correo);
		setFechaNacimiento(fechaNacimiento);
	}
	
	
	public Persona() {
		this(new Nif(), 
				"Invitado", 
				"Invitado Invitado", 
				new DireccionPostal(),
				new Correo(), 
				new Fecha().addYears(-16)
				);
	}

	/**
	 * Constructor copia.
	 * @param usr
	 */
	public Persona(Persona persona) {
		this(new Nif(persona.nif),
				new String(persona.nombre),
				new String(persona.apellidos),
				new DireccionPostal(persona.domicilio),
				new Correo(persona.correo),
				new Fecha(persona.fechaNacimiento)
			);
	}
	
	public Nif getNif() {
		return nif;
	}
	
	public void setNif(Nif nif) {
		assert nif != null;
		this.nif = nif;	
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {	
		if (nombreValido(nombre)) {
			this.nombre = nombre;
			return;
		} 
		throw new ModeloException ("Persona.nombre: null o Formato no válido...");
	}

	/**
	 * Comprueba validez del nombre.
	 * @param nombre.
	 * @return true si cumple.
	 */
	protected boolean nombreValido(String nombre) {
		return nombre!= null && nombre.matches(Formato.PATRON_NOMBRE_PERSONA);
		}


	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		if (apellidosValidos(apellidos)) {
			this.apellidos = apellidos;
			return;
		} 
		throw new ModeloException ("Persona.apellidos: null o Formato no válido...");
	}

	/**
	 * Comprueba validez de los apellidos.
	 * @param apellidos.
	 * @return true si cumple.
	 */
	protected boolean apellidosValidos(String apellidos) {
		return apellidos != null && apellidos.matches(Formato.PATRON_APELLIDOS);	
	}

	public DireccionPostal getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(DireccionPostal domicilio) {
		assert domicilio != null;
		this.domicilio = domicilio;
		
	}

	public Correo getCorreo() {
		return correo;
	}

	public void setCorreo(Correo correo) {
		assert correo != null;
		this.correo = correo;
	}

	public Fecha getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Fecha fechaNacimiento) {
		if (fechaNacimientoValida(fechaNacimiento)) {
			this.fechaNacimiento = fechaNacimiento;
			return;
			} 
			throw new ModeloException ("Persona.FechaNacimiento: null o no válida...");
	}

	/**
	 * Comprueba validez de una fecha de nacimiento.
	 * @param fechaNacimiento.
	 * @return true si cumple.
	 */
	protected boolean fechaNacimientoValida(Fecha fechaNacimiento) {
		return fechaNacimiento != null && !fechaNacimiento.after(new Fecha().addYears(-16));
		//return fechaNacimiento.difYears(new Fecha().addYears(-16)) <= 0;
	}
	
	/**
	 * Redefine el método heredado de la clase Objecto.
	 * @return el texto formateado del estado -valores de atributos- de objeto de la clase Usuario.  
	 */
	@Override
	public String toString() {
		return String.format(
				  "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n", 
				"nif:", this.nif, 
				"nombre:", this.nombre, 
				"apellidos:", this.apellidos,  
				"domicilio:", this.domicilio, 
				"correo:", this.correo, 
				"fechaNacimiento:", (this.fechaNacimiento.getYear()) + "." 
						+ (this.fechaNacimiento.getMes()) + "." 
						+ this.fechaNacimiento.getDia()	
				
		);		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellidos == null) ? 0 : apellidos.hashCode());
		result = prime * result + ((correo == null) ? 0 : correo.hashCode());
		result = prime * result + ((domicilio == null) ? 0 : domicilio.hashCode());
		result = prime * result + ((fechaNacimiento == null) ? 0 : fechaNacimiento.hashCode());
		result = prime * result + ((nif == null) ? 0 : nif.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (apellidos == null) {
			if (other.apellidos != null)
				return false;
		} else if (!apellidos.equals(other.apellidos))
			return false;
		if (correo == null) {
			if (other.correo != null)
				return false;
		} else if (!correo.equals(other.correo))
			return false;
		if (domicilio == null) {
			if (other.domicilio != null)
				return false;
		} else if (!domicilio.equals(other.domicilio))
			return false;
		if (fechaNacimiento == null) {
			if (other.fechaNacimiento != null)
				return false;
		} else if (!fechaNacimiento.equals(other.fechaNacimiento))
			return false;
		if (nif == null) {
			if (other.nif != null)
				return false;
		} else if (!nif.equals(other.nif))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
	

}