package modelo;

public interface Identificable {

	String getId();

}
