/** 
Proyecto: Juego de la vida.
 * Clase de excepcion para la gestion de los errpres en las clases del modelo.
 * @since: prototipo 0.1.2
 * @source: DatosException.java 
 * @version: 0.1.2 - 2020/01/07
 * @author: JGR
 */

package modelo;

public class ModeloException extends RuntimeException {

	public ModeloException(String mensaje) {
		super(mensaje);
	}
	
	public ModeloException() {
		super();
	}
}
