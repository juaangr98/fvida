/** 
 * Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 
 * En esta versión sólo se ha aplicado un diseño OO básico.	
 * @since: prototipo 0.1.0
 * @source: Posicion.java 
 * @version: 0.1.2 - 2020/02/07
 * @author: JGR
 */

package modelo;

public class Posicion {
	private int x;
	private int y;
}

