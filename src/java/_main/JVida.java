/** 
Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 
 * En esta versión sólo se ha aplicado un diseño OO básico.	
 * @since: prototipo 0.1.0
 * @source: JVida.java 
 * @version: 0.1.2 - 2020/02/07
 * @author: JGR
 */

package _main;

import accesoDatos.Datos;
import accesoUsr.Presentacion;
import modelo.Sesion;
import modelo.Simulacion;
import util.Fecha;


public class JVida {

	private static Presentacion interfazUsr;
	private static Datos almacen;
	
	public JVida() {
		interfazUsr = new Presentacion();
		almacen = Datos.getAlmacen();
	}
	
	/**
	 * Secuencia principal del programa.
	 */
	public static void main(String[] args) {		
		
		new JVida();	
		almacen.mostrarTodosUsuarios();
		
		if (interfazUsr.inicioSesionCorrecto()) {
				
			System.out.println("Sesión: " + almacen.getSesionesRegistradas() + '\n' + "Iniciada por: " 
					+ interfazUsr.getSesion().getUsr().getNombre() + " " 
					+ interfazUsr.getSesion().getUsr().getApellidos());	
			
			Simulacion demo = almacen.buscarSimulacion("Demo");
			interfazUsr.setSimulacion(demo);
			demo.getMundo().lanzarSimulacion(interfazUsr);
		}
		else {
			System.out.println("\nDemasiados intentos fallidos...");
		}		
		System.out.println("Fin del programa.");
	}

} 
