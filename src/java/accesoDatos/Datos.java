/** 
Proyecto: Juego de la vida.
 * Implementa el almacén de datos.
 * @since: prototipo 0.1.1
 * @source: Datos.java 
 * @version: 0.1.2 - 2020/01/07
 * @author: JGR
 */

package accesoDatos;

import java.util.ArrayList;
import java.util.HashMap;

import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Mundo;
import modelo.Nif;
import modelo.Sesion;
import modelo.Simulacion;
import modelo.Usuario;
import modelo.Usuario.RolUsuario;
import util.Fecha;
import util.Formato;


public class Datos {

	private static Datos almacen = new Datos();
	private  ArrayList <Identificable> datosUsuarios;
	private  ArrayList <Identificable> datosSesiones;
	private  ArrayList<Identificable> datosSimulaciones;
	private  ArrayList<Identificable> datosMundos;
	private HashMap <String, String> equivalenciasId;

	public Datos() {
		datosUsuarios = new ArrayList <Identificable> ();
		datosSesiones = new ArrayList <Identificable> ();
		datosSimulaciones = new ArrayList <Identificable> ();
		datosMundos = new ArrayList <Identificable> ();
		equivalenciasId = new HashMap<String, String>();
		this.iniciarAlmacen();
	}

	private void iniciarAlmacen() {
		cargarUsuariosIntegrados();
		cargarSimulacionDemo();
	}

	public static Datos getAlmacen() {
		return almacen;
	}

	private int IndexSort(ArrayList <Identificable> datos, String id) {
		int inicio = 0;
		int fin = datos.size() - 1;
		while (inicio <= fin) {
			int medio = (inicio+fin) / 2;
			int comparacion = datos.get(medio).getId().compareTo(id);

			if (comparacion == 0) {
				return medio+1;
			}
			if (comparacion < 0) {
				inicio = medio+1;
			}
			else {
				fin = medio-1;
			}
		}
		return -(fin+2); 		// Indices base 1
	}


	//USUARIOS
	///////////

	public Usuario obtenerUsuario(String id) {
		int index = IndexSort(datosUsuarios, equivalenciasId.get(id));
		if (index < 0) {
			return null;
		}
		return (Usuario) datosUsuarios.get(index-1);
	}

	public Usuario getUsuario(int indice) {
		return (Usuario) datosUsuarios.get(indice);
	}

	public void altaUsuario(Usuario usr) {
		int index = IndexSort(datosUsuarios, usr.getId());
		if (index < 0) {
			// index es en base 1
			datosUsuarios.add(-index-1, usr);
			altaEquiId(usr);
		}
		else {

			int cont = 1;
			do {
				cont ++;
				usr.VariarId();
				index = IndexSort(datosUsuarios, usr.getId());
				if (index < 0) {
					// index es en base 1
					datosUsuarios.add(-index-1, usr);
					altaEquiId(usr);
					return;
				}
			}
			while (cont < Formato.LETRAS_NIF.length());
			throw new DatosException("Datos.altaUsuario: Id en uso...");
		}
	}

	private void altaEquiId (Usuario usr) {

		equivalenciasId.put(usr.getId(), usr.getId());
		equivalenciasId.put(usr.getNif().getTexto(), usr.getId());
		equivalenciasId.put(usr.getCorreo().getTexto(), usr.getId());
	}

	public Usuario bajaUsuario(String id) {
		int index = IndexSort(datosUsuarios, id);
		if (index > 0) {
			Usuario usr  = (Usuario) datosUsuarios.get(-index-1);
			// index es en base 1 
			datosUsuarios.remove(index-1);
			bajaEquiId(usr);
			return usr;
		}
			throw new DatosException("Datos.bajaUsuario: no existe...");
	}

	private void bajaEquiId(Usuario usr) {

		equivalenciasId.remove(usr.getId());
		equivalenciasId.remove(usr.getNif().getTexto());
		equivalenciasId.remove(usr.getCorreo().getTexto());
	}

	public Usuario modificarUsuario(Usuario usr) {
		int index = IndexSort(datosUsuarios, usr.getId());
		if (index > 0) {
			Usuario aux  = (Usuario) datosUsuarios.get(-index-1);
			// index es en base 1 
			datosUsuarios.set(index-1, usr);
			bajaEquiId(aux);
			altaEquiId(usr);
			return aux;
		}
			throw new DatosException("Datos.modificarUsuario: el usuario no existe...");
	}

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public void mostrarTodosUsuarios() {
		for (Identificable u: datosUsuarios) {
			System.out.println("\n" + u);
		}
	}

	private void cargarUsuariosIntegrados() {

		Usuario admin = null;

		try {
			Usuario invitado = new Usuario();
			altaUsuario(invitado);


			admin = new Usuario(new Nif("00000001R"),
					"Admin",
					"Admin Admin",
					new DireccionPostal(),
					new Correo("admin@correo.es"),
					new Fecha().addYears(-16),
					new Fecha(),
					new ClaveAcceso("Miau#0"),
					Usuario.RolUsuario.ADMIN
					);
			altaUsuario(admin);
		} 
		catch (ModeloException e) {
			e.printStackTrace();
		} 
		catch (DatosException e) {
			e.printStackTrace();
		} 
	}

	public int getUsuariosRegistrados() {
		return datosUsuarios.size();
	}

	//SESIONES
	///////////

	/**
	 * Busca indice de sesion dado su identificador.
	 * @param id - el identificador de la sesión a buscar.
	 * @return - la Sesion encontrada o null si no existe.
	 */
	private int indexSort(ArrayList <Identificable> datos, String id) {
		int inicio = 0;
		int fin = datos.size() - 1;
		while (inicio <= fin) {
			int medio = (inicio+fin) / 2;
			int comparacion = datos.get(medio).getId().compareTo(id);

			if (comparacion == 0) {
				return medio+1;
			}
			if (comparacion < 0) {
				inicio = medio+1;
			}
			else {
				fin = medio-1;
			}
		}
		return -(fin+2); // Indices base 1
	}

	/**
	 * Registro de la sesión de usuario.
	 */
	public void altaSesion(Sesion sesion) {
		int index = indexSort(datosSesiones, sesion.getId());
		if(index < 0) {
			datosSesiones.add(sesion);
			return;
		}
			throw new DatosException("Datos.altaSesion: el id está en uso...");
	}

	public Sesion bajaSesion(String id) {
		int index = IndexSort(datosSesiones, id);
		if (index > 0) {
			Sesion sesion  = (Sesion) datosSesiones.get(-index-1);
			// index es en base 1 
			datosSesiones.remove(index-1);

			return sesion;
		}
		throw new DatosException("Datos.bajaSesion: no existe...");
	}

	public Sesion modificarSesion(Sesion sesion) {
		int index = IndexSort(datosSesiones, sesion.getId());
		if (index > 0) {
			Sesion aux  = (Sesion) datosSesiones.get(-index-1);
			// index es en base 1 
			datosSesiones.set(index-1, sesion);

			return aux;
		}
		throw new DatosException("Datos.modificarSesion: no existe...");
	}

	public int getSesionesRegistradas() {
		return datosSesiones.size();
	}

	//SIMULACIONES
	//////////////

	/**
	 * Busca Simulacion dado su identificador.
	 * @param id - el identificador de la simulación a buscar.
	 * @return - la Simulacion encontrada o null si no existe.
	 */
	public Simulacion buscarSimulacion(String id) {
		if (id.equals("Demo")) {
			return (Simulacion) datosSimulaciones.get(0);
		}
		for (Identificable simulacion : datosSimulaciones) {
			if (simulacion != null && simulacion.getId().equalsIgnoreCase(id)) {
				return (Simulacion) simulacion;
			}
		}
		return null;                        // No encuentra.
	}

	/**
	 * Registro de la simulación.
	 */
	public void altaSimulacion(Simulacion simulacion) {
		int index = indexSort(datosSimulaciones, simulacion.getId());
		if(index < 0) {
			datosSimulaciones.add(simulacion);
			return;
		}
		throw new DatosException("Datos.altaSimulacion: el id está en uso...");
	}

	public Simulacion bajaSimulacion(String id) {
		int index = IndexSort(datosSimulaciones, id);
		if (index > 0) {
			Simulacion simulacion  = (Simulacion) datosSimulaciones.get(-index-1);
			// index es en base 1 
			datosSimulaciones.remove(index-1);

			return simulacion;
		}
		throw new DatosException("Datos.bajaSimulacion: la simulacion no existe...");
	}

	public Simulacion modificarSimulacion(Simulacion simulacion) {
		int index = IndexSort(datosSimulaciones, simulacion.getId());
		if (index > 0) {
			Simulacion aux  = (Simulacion) datosSimulaciones.get(-index-1);
			// index es en base 1 
			datosSimulaciones.set(index-1, simulacion);

			return aux;
		}
		throw new DatosException("Datos.modificarSimulacion: la simulacion no existe...");
	}

	public int getSimulacionesRegistradas() {
		return datosSimulaciones.size();
	}

	private void cargarSimulacionDemo() {
		Simulacion simulacionDemo = null;
		Mundo mundoDemo = null;
		try {
			simulacionDemo = new Simulacion();
		} catch (ModeloException e) {
			e.printStackTrace();
		}
		mundoDemo = new Mundo();
		mundoDemo.setEspacio(cargarMundoDemo());  
		simulacionDemo.setMundo(mundoDemo);
		altaSimulacion(simulacionDemo);
	}

	/**
	 * Carga datos demo en la matriz que representa el mundo.
	 */
	private byte[][] cargarMundoDemo() {
		// En este array los 0 indican celdas con células muertas y los 1 vivas.
		byte[][] espacioMundo = new byte[][] {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 1x Planeador
			{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 1x Flip-Flop
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 1x Still Life
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }  //
		};
		return espacioMundo;
	}

	//Mundos
	///////////
	
}