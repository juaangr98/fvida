/** 
Proyecto: Juego de la vida.
 * Clase de excepcion para la gestion de los errpres en las clases del modelo.
 * en el almacen de datos.
 * @since: prototipo 0.1.2
 * @source: ModeloException.java 
 * @version: 0.1.2 - 2020/01/07
 * @author: JGR
 */

package accesoDatos;

public class DatosException extends RuntimeException {

	public DatosException(String mensaje) {
		super(mensaje);
	}
	
	public DatosException() {
		super();
	}
}
