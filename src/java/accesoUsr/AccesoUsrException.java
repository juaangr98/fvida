/** 
Proyecto: Juego de la vida.
 * Clase de excepcion para la gestion de los errpres en las clases del modelo.
 * @since: prototipo 0.1.2
 * @source: AccesoUsrException.java 
 * @version: 0.1.2 - 2020/01/07
 * @author: JGR
 */

package accesoUsr;

public class AccesoUsrException extends RuntimeException {

	public AccesoUsrException(String mensaje) {
		super(mensaje);
	}
	
	public AccesoUsrException() {
		super();
	}
}
