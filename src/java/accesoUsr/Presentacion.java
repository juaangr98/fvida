/** 
Proyecto: Juego de la vida.
 * Implementa el interfaz de usuario.
 * @since: prototipo 0.1.1
 * @source: Presentacion.java 
 * @version: 0.1.2 - 2020/01/07
 * @author: JGR
 */

package accesoUsr;

import java.util.Scanner;

import accesoDatos.Datos;
import modelo.ClaveAcceso;
import modelo.ModeloException;
import modelo.Sesion;
import modelo.Simulacion;
import modelo.Usuario;
import util.Criptografia;

public class Presentacion {
	
	private static final int MAX_INTENTOS_FALLIDOS = 3;
	private Sesion sesion;
	private Simulacion simulacion;
	
	public Presentacion()  {
		try {
			simulacion =  new Simulacion();
		} catch (ModeloException e) {
			e.printStackTrace();
		}
	}

	public Sesion getSesion() {
		return sesion;
	}

	public Simulacion getSimulacion() {
		return simulacion;
	}
	
	public void setSimulacion(Simulacion simulacion) {
		this.simulacion = simulacion;
	}
	
	//
	//SESIÓN
	////////
	
	/**
	 * Controla el acceso de usuario.
	 * @return true si la sesión se inicia correctamente.
	 */
	public boolean inicioSesionCorrecto() {
		Scanner teclado = new Scanner(System.in);			// Entrada por consola.
		int intentosPermitidos = MAX_INTENTOS_FALLIDOS;

		do {
			// Pide usuario y contraseña.
			System.out.print("Introduce el id de usuario: ");
			String id = teclado.nextLine();
			System.out.print("Introduce clave acceso: ");
			String clave = teclado.nextLine();

			// Busca en el almacén el usuario coincidente con las credenciales.
			Usuario usrEnSesion = Datos.getAlmacen().obtenerUsuario(id);
			
			if (usrEnSesion != null 
					&& usrEnSesion.getClaveAcceso().getTexto().equals(Criptografia.cesar(clave))) {
				sesion = new Sesion(usrEnSesion);
				Datos.getAlmacen().altaSesion(sesion);
				return true;
			} 
			else {
				intentosPermitidos--;
				System.out.print("Credenciales incorrectas: ");
				System.out.println("Quedan " + intentosPermitidos + " intentos... ");
			} 
		} while (intentosPermitidos > 0);

		return false;
	}
	
	//
	//SIMULACIÓN
	////////////
	
	/**
	 * Despliega en la consola el estado almacenado, corresponde
	 * a una generación del Juego de la Vida.
	 */
	public void mostrarMundo(int generacion) {
		System.out.println("\nGeneración: " + generacion);
		for (int i = 0; i < simulacion.getMundo().getEspacio().length; i++) {
			for (int j = 0; j < simulacion.getMundo().getEspacio()[i].length; j++) {		
				System.out.print((simulacion.getMundo().getEspacio()[i][j] == 1) ? "|o" : "| ");
			}
			System.out.println("|");
		}
	}
}
